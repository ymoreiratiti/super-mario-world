const sceneConfig: Phaser.Types.Scenes.SettingsConfig = {
  active: false,
  visible: false,
  key: 'Game',
};

export class GameScene extends Phaser.Scene {
  public speed = 200;

  private cursors: Phaser.Types.Input.Keyboard.CursorKeys;
  private image: Phaser.Physics.Arcade.Sprite;
  private player: Phaser.Physics.Arcade.Sprite;
  private ground: Phaser.Tilemaps.StaticTilemapLayer;

  constructor() {
    super(sceneConfig);
  }

  /**
   *
   */
  public create (): void {
    this.cameras.main.setBounds(0, 0, 3392, 100);
    // this.physics.world.setBounds(0, 0, 3392, 240);

    const map = this.make.tilemap({ key: 'map' });
    const tileset = map.addTilesetImage('tiles_grassland', 'tiles_grassland');
    this.ground = map.createStaticLayer('Ground', tileset, 0, 0);
    this.ground.setCollisionByExclusion([-1]);

    this.cursors = this.input.keyboard.createCursorKeys();

    this.setupPlayer();

    this.physics.add.collider(this.player, this.ground);

    this.cameras.main.startFollow(this.player, true, 0.08, 0.08);
  }

  /**
   *
   */
  private setupPlayer () {
    this.player = this.physics.add.sprite(100, 100, 'mario_small', 'Idle');

    this.player.setCollideWorldBounds(true);

    this.anims.create({
      key: 'mario_small_walk',
      frames: this.anims.generateFrameNames('mario_small', {
        prefix: 'Walk',
        end: 2,
        zeroPad: 2,
      }),
      frameRate: 9,
    });
  }

  /**
   *
   */
  public update (): void {
    this.player.setVelocity(0);

    if (this.cursors.left.isDown) {
      this.player.setFlipX(false).setVelocityX(-125);
      this.player.anims.play('mario_small_walk', true);
    }

    if (this.cursors.right.isDown) {
      this.player.setFlipX(true).setVelocityX(125);
      this.player.anims.play('mario_small_walk', true);
    }

    if (this.cursors.up.isDown) {
      this.player.setVelocityY(-200);
    } else if (this.cursors.down.isDown) {
      this.player.setVelocityY(200);
    }
  }
}
