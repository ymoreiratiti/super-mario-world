import * as Phaser from 'phaser';
import Scenes from './scenes';

const gameConfig: Phaser.Types.Core.GameConfig = {
  title: 'Super Mario World HTML5',

  type: Phaser.AUTO,
  parent: 'game',

  width: 448,
  height: 336,

  scene: Scenes,

  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 10000 },
      debug: true,
    },
  },

  backgroundColor: '#000000',
};

export const game = new Phaser.Game(gameConfig);
